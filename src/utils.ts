import {Vector3, VertexData} from "babylonjs";

// https://doc.babylonjs.com/snippets/line2d#draw-a-line-of-given-width
export function getLineVertexData(path: any, width: number): VertexData {

	const positions = [];
	const indices = [];
	const outerData = [];
	const innerData = [];

	let angle: number = 0;

	width *= 0.5;

	const nbPoints: number = path.length;
	let line: Vector3 = Vector3.Zero();
	path[1].subtractToRef(path[0], line);
	const nextLine = Vector3.Zero();

	let direction: number = 0;
	let lineNormal: Vector3 = new Vector3(-line.z, 0, line.x).normalize();
	line.normalize();

	innerData[0] = path[0].subtract(lineNormal.scale(width));
	outerData[0] = path[0].add(lineNormal.scale(width));

	for (let p: number = 0; p < nbPoints - 2; p++) {
		path[p + 2].subtractToRef(path[p + 1], nextLine);
		const dot: number = floatPrecision(Vector3.Dot(line, nextLine));
		const length: number = floatPrecision(line.length() * nextLine.length());
		const acos: number = Math.acos(dot / length);

		angle = Math.PI - acos;

		direction = Vector3.Cross(line, nextLine).normalize().y;
		lineNormal = new Vector3(-line.z, 0, line.x).normalize();
		line.normalize();
		innerData[p + 1] = path[p + 1].subtract(lineNormal.scale(width)).subtract(line.scale(direction * width / Math.tan(angle / 2)));
		outerData[p + 1] = path[p + 1].add(lineNormal.scale(width)).add(line.scale(direction * width / Math.tan(angle / 2)));
		line = nextLine.clone();
	}
	if (nbPoints > 2) {
		path[nbPoints - 1].subtractToRef(path[nbPoints - 2], line);
		lineNormal = new Vector3(-line.z, 0, line.x).normalize();
		line.normalize();
		innerData[nbPoints - 1] = path[nbPoints - 1].subtract(lineNormal.scale(width));
		outerData[nbPoints - 1] = path[nbPoints - 1].add(lineNormal.scale(width));
	} else {
		innerData[1] = path[1].subtract(lineNormal.scale(width));
		outerData[1] = path[1].add(lineNormal.scale(width));
	}

	let maxX = Number.MIN_VALUE;
	let minX = Number.MAX_VALUE;
	let maxZ = Number.MIN_VALUE;
	let minZ = Number.MAX_VALUE;

	for (let p: number = 0; p < nbPoints; p++) {
		positions.push(innerData[p].x, innerData[p].y, innerData[p].z);
		maxX = Math.max(innerData[p].x, maxX);
		minX = Math.min(innerData[p].x, minX);
		maxZ = Math.max(innerData[p].z, maxZ);
		minZ = Math.min(innerData[p].z, minZ);
	}

	for (let p: number = 0; p < nbPoints; p++) {
		positions.push(outerData[p].x, outerData[p].y, outerData[p].z);
		maxX = Math.max(innerData[p].x, maxX);
		minX = Math.min(innerData[p].x, minX);
		maxZ = Math.max(innerData[p].z, maxZ);
		minZ = Math.min(innerData[p].z, minZ);
	}

	for (let i: number = 0; i < nbPoints - 1; i++) {
		indices.push(i, i + 1, nbPoints + i + 1);
		indices.push(i, nbPoints + i + 1, nbPoints + i)
	}

	const vertexData: VertexData = new VertexData();

	// Assign positions and indices to vertexData
	vertexData.positions = positions;
	vertexData.indices = indices;

	return vertexData;
}

export function floatPrecision(float: number, precision: number = 7): number {
	return parseFloat(float.toFixed(precision))
}
