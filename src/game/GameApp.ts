import {Engine} from "babylonjs";
import GameScene from "./GameScene";

export class GameApp {

	private readonly _engine: Engine = null;
	private readonly _gameScene: GameScene = null;

	constructor() {
		const canvas: any = document.getElementById("renderCanvas");
		this._engine = new Engine(canvas, true, {preserveDrawingBuffer: true, stencil: true});
		this._gameScene = new GameScene(canvas, this._engine);

		this._engine.runRenderLoop(() => this._render());

		window.addEventListener("resize", () => this._engine.resize());
	}

	private _render(): void {
		this._gameScene.render();
	}
}
