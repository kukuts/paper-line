import {
	Axis,
	Engine,
	FollowCamera,
	HemisphericLight,
	Mesh,
	Scene,
	Space,
	TargetCamera,
	Vector3,
	VirtualJoystick,
	StandardMaterial,
} from "babylonjs";
import {getLineVertexData} from "../utils";

export default class GameScene extends Scene {

	private readonly FOLLOW_CAMERA_OFFSET: Vector3 = new Vector3(0, 25, -25);
	private readonly OBJECT_MOVE_SPEED: number = 0.1;
	private readonly OBJECT_ROTATE_SPEED: number = 4;
	private readonly GAME_OBJECT_SIZE: number = 1;
	private readonly PATH_DRAW_DELAY: number = 0.07;
	private readonly JOYSTICK_SENS: number = 3;

	private readonly _canvas: any = null;
	private readonly _camera: TargetCamera = null;
	private readonly _joystick: VirtualJoystick = null;

	private readonly _pathArr: Array<Vector3>;
	private readonly _pathObject: Mesh = null;
	private readonly _gameObject: Mesh = null;

	private _drawDelay: number = 0;

	constructor(canvas: any, engine: Engine) {
		super(engine);

		this._canvas = canvas;
		this._camera = this._createCamera();
		this._joystick = this._createJoystick();

		new HemisphericLight("sceneLight", new Vector3(0, 1, 0), this);

		this._gameObject = Mesh.CreateBox("gameObject", this.GAME_OBJECT_SIZE, this);
		this._gameObject.position.y = 1;

		this._pathObject = new Mesh("pathObject", this);
		this._pathObject.material = new StandardMaterial("pathMaterial", this);
		// this._pathObject.material.wireframe = true;
		this._pathArr = [];

		this._camera.position = this.FOLLOW_CAMERA_OFFSET;
		this._camera.setTarget(this._gameObject.position);

		this._processObjectPath(0, true);
		this.registerBeforeRender(() => this._beforeRender());
	}

	private _beforeRender(): void {
		if (!!this._gameObject && !!this._camera) {
			const deltaTime: number = this.getEngine().getDeltaTime() / 1000;
			const deltaPos: Vector3 = this._joystick.deltaPosition;
			const direction: Vector3 = new Vector3(deltaPos.x, 0, deltaPos.y);

			this._gameObject.translate(this._gameObject.forward, this.OBJECT_MOVE_SPEED, Space.WORLD);

			const angle: number = Vector3.GetAngleBetweenVectors(this._gameObject.forward, direction, Axis.Y);
			if (!isNaN(angle) && direction.length()) {
				this._gameObject.rotate(Axis.Y, angle * deltaTime * this.OBJECT_ROTATE_SPEED, Space.WORLD);
			}

			this._camera.position = this._gameObject.position.add(this.FOLLOW_CAMERA_OFFSET);

			this._processObjectPath(deltaTime);
		}
	}

	private _processObjectPath(dt: number, force: boolean = false): void {
		this._drawDelay += dt;
		if (force || this._drawDelay >= this.PATH_DRAW_DELAY) {
			const pathPt: Vector3 = this._gameObject.position.clone();
			pathPt.y -= this.GAME_OBJECT_SIZE * 0.5;
			this._pathArr.push(pathPt);
			if (this._pathArr.length > 1) {
				getLineVertexData(this._pathArr, this.GAME_OBJECT_SIZE).applyToMesh(this._pathObject);
			}
			this._drawDelay = 0;
		}
	}

	private _createJoystick(): VirtualJoystick {
		const joystick: VirtualJoystick = new VirtualJoystick(true);
		joystick.setJoystickSensibility(this.JOYSTICK_SENS);
		return joystick;
	}

	private _createCamera(): TargetCamera {
		const camera: FollowCamera = new FollowCamera("camera", Vector3.Zero(), this);
		return camera;
	}

}
