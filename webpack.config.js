const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const webpackConfig = {
	context: __dirname,
	output: {
		path: path.resolve(__dirname, "bin"),
		filename: "js/[name].js"
	},
	entry: {
		main: "./src/app.ts"
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: "ts-loader",
				exclude: /node_modules/,
			},
		]
	},
	devtool: "source-map",
	plugins: [
		new HtmlWebpackPlugin({
			inlineSource: ".(js|css)$",
			title: "Paper line",
			template: "template/index.html",
			filename: "index.html",
			hash: false,
		}),
	],
	resolve: {
		extensions: [".js", ".ts", ".css", ".json"],
		modules: ["node_modules"]
	}
};
module.exports = () => webpackConfig;
